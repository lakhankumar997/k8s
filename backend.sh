#!/bin/bash

sudo docker container stop backend
sudo docker container rm backend
sudo docker image rm backend
cd backend/
sudo docker build -t backend:latest .
sudo docker run -p 8000:8000 -d --name=backend --link mysql:mysql backend:latest
