#!/bin/bash

sudo docker container stop frontend
sudo docker container rm frontend
sudo docker image rm frontend
cd frontend/
sudo docker build -t frontend:latest .
sudo docker run -p 80:80 -d --name=frontend --link backend:backend  frontend:latest
