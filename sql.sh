#!/bin/bash

sudo docker container stop mysql
sudo docker container rm mysql
sudo docker image rm mysql
cd sql/
sudo docker build -t mysql:latest .
sudo docker run -p 3306:3306 -d --name=mysql mysql:latest
