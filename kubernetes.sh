#!/bin/bash

kubectl get nodes
kubectl delete deployment backend
kubectl delete deployment frontend
kubectl delete deployment database
kubectl apply -f backend.yml
kubectl apply -f frontend.yml
kubectl apply -f database.yml
